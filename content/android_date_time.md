Title: Set date and time from adb
Date: 2016-04-18 00:18
Category: Android
Tags: Android, Android Studio, Mobile

#Set date and time from adb

adb shell date -s YYYYMMDD.HHmmss
adb shell date -s 20141020.165800

adb shell date -s 20120423.130000
adb shell 'su 0 date -s YYYYMMDD.HHmmss'
