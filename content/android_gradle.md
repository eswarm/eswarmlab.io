Title: Android Studio stuck on “Gradle: resolve dependancies'
Date: 2016-04-18 00:10
Category: Android
Tags: Android, Android Studio, Mobile

#Android Studio stuck on “Gradle: resolve dependancies'



* Check all the dependencies, whether they are valid or not.


* Or for a temporary fix, try offline mode.

> To enable this setting go to:
> Preferences -> Gradle
> In the right side options go down to "Global Gradel Settings" and check the "Offline work" box.

* Build from the command line to know the exact source of the problem

 In windows

 > gradlew.bat assembleDebug

 In linux

>  chmod +x gradlew
> ./gradlew assembleDebug

* If nothing works, You can also try

Delete all contents of folder C:\Users\<Username>\.gradle
