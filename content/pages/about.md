Title: About

Software engineer by trade, working on mobiles since Symbian.I primarily deal with Android currently. I am a polyglot programmer who can dabble in C++, Java, Python, Javascript. Platforms or frameworks I have worked on are Symbian, Android, Qt, AngularJS, Ionic, Spring. I am pragmatic with respect to technology and believe in using the best tool for the job.

I have more than a passing interest in UX, and strive to deliver the best experience possible.

My other interests are carpentry and electronics though I can still be best called a noob in both.
