Title: Projects

[focustrack.in](http://focustrack.in)  
Focustrack.in is a productivity app that helps you keep track of the time spent with your devices.  
[Android App available](https://play.google.com/store/apps/details?id=in.focustrack.stock) Windows desktop app coming soon.
[Visit focustrack.in for more details](http://focustrack.in)

[Somabot](https://github.com/eswarm/somabot)  
An automatic bartender built using libmraa, you can use it on your raspberry PI, beaglebone, Intel Galileo/Edison
[More details here]({filename}/pages/somabot.md)

[materialistic-pelican](https://github.com/eswarm/materialistic-pelican)  
A theme based on Material Design Lite(from Google) for Pelican based sites.

[AutoSortD](https://github.com/eswarm/AutoSortD)  
Automatically sort your directories.(Work in progress)   
